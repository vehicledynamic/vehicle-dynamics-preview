# Vehicle Dynamics - Awesome Space Pirates Team #

# [Project Report](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/src/26963ab175dc2afb9024c48eac367b1989324395/report.pdf?at=master "Project Report") #

![Logo](http://logos-vector.com/images/logo/lar/1/0/2/102885/GP2_series_575ae_250x250.png)

## Pagina di progetto ##

Benvenuti nella pagina del progetto di **Vehicle Dynamic** di:

 * Mattia Veso 
 * Jacopo Planchestainer
 * Matteo Ragni

In questo documento troverete tutte le informazioni rigurdanti il progetto e la sua organizzazione generale. Il repository Ã¨ da considerarsi **privato**, **non commerciale** ed Ã¨ **severamente vietata ogni copia non autorizzata**. I file contenuti al suo interno sono coperti da copyright da parte dei legittimi proprietari.

## Struttura del progetto ##

Le directory del progetto sono organizzate in modo da mantenere un certo ordine. Alcuni file (principalmente `*.m`, `*.mla` e `*.maple`) vengono generati automaticamente da parte degli script di modellazione analitica. 

Le directory sono:

 * `.git`: contiene la configurazione del repo. 
 * `Junk`: al suo interno sono salvati vecchie versioni di alcuni script che ci sembrava importante salvare prima di effettuare estese modifiche. Al suo interno si trovano anche alcuni file di testo utilizzati come promemoria e la prima versione del progetto **Matlab**.
 * `Origins`: all'interno di questa directory si trovano alcuni dei file originali scritti dal prof. Ing. Francesco Biral. I file al suo interno sono stati modificati/alterati per prove e implementazioni del nostro progetto.
 * `Papers`: all'interno ci sono documentazioni e pubblicazioni di chi ha affrontato il problema di __minimum lap time__ prima di noi. I paper sono stati scaricati mediante account universitari e **non possono essere distributi**.
 * `Maple`: all'interno di questa directory ci sono gli script **Maple 17** che descrivono la versione analitica del modello **single track** del veicolo.
 * `Matlab`: all'interno di questa directory sono contenuti gli script **Matlab r2013** che descrivono la versione analitica e il problema di ottimizzazione del modello.

## Alcuni risultati ##

Di seguito alcune immagini rappresentative di quello che abbiamo fatto:

 * Rappresentazione dell'algoritmo automatico di meshing del tracciato. L'algoritmo si basa sulla integrazione Runge Kutta, che sceglie automaticamente la dimensione di meshing sulla base del differenziale di ingresso _k(s)_
   ![Track](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/track.png)
 
 * Calcolo del G-G plot per il veicolo selezionato. Il risultato deriva da una serie di ottimizzazioni a cascata eseguita per accelerazioni longitudinali positive e negative, a loro volta iterato su un vettore di velocitÃ , dalla velocitÃ  minima alla velocitÃ  massima raggiungibile dal veicolo. L'ottimizzazione Ã¨ eseguita su un modello *quasi steady state* e i punti di risultato sono interpolati da una funzione simmetrica a 2 dimensioni di ordine 6, sfruttando l'operatore di Kronecher e la inversione della pseudoinversa associata per minimizzare il problema ai minimi quadrati. La funzione ottenuta, specchiata rispetto all'asse VG,Ax per ottenere la funzione sulle accelerazioni laterali negative. Di seguito il risultato:
   ![Ax Max](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/Ax_max.png)
   ![Ax Min](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/Ax_min.png)
   ![GGplot](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/ggplot.png)
 
 * Sono state implementate diverse procedure per il calcolo del Minimum Time Manouver sulla chicane alla nona e decima curva del tracciato. le diverse ottimizzazioni ci hanno fornito risultati molto diversi:
 ![Simulazioni MTM](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/ottimizzazione_tracciato.png)
 ![Simulazioni MTM](https://bitbucket.org/vehicledynamic/vehicle-dynamics-preview/raw/9771296b009a8250f8b3393ada4265c73b551391/ottimizzazione.png)

## Modello Maple ##

Il modello Maple Ã¨ contenuto nella directory `Maple`, all'interno della quale troviamo diverse directory che suddividono le varie parti del progetto. Il modello genera delle librerie che contengono delle funzioni che sono state utilizzate nel codice. I dati, in varie forme si trovano nella directory `Data` (esempio, mappa del motore), mentre i  generatori di libreria sono all'interno della directory `LibsGenerator`. All'interno di questa directory si riconoscono i seguenti script:

 * `EngineLib` e `EngineLibWin` (obsoleto): questo script provvede alla generazione della libreria che interpola la mappa del motore. Al suo interno, la variabile `inputFile` determina la posizione relativa del file di testo che verrÃ  utilizzato per interpolare la mappa del motore.
 * `TireLib_v2`: Descrive il modello di Pacejka per due pneumatici da gara. La libreria esporta diverse funzioni che calcolano direttamente il valore delle forze combinate e i coefficienti che permettono di calcolare tali forze sono automaticamente sostituiti. Inoltre la libreria ritorna due prototipi di funzione, il modello di Pacejka completo e la sua versione semplificata.
 * `VehicleData2W`: esporta un file `*.maple` e uno script `*.m` che definiscono tutti i parametri e i dati del veicolo. Al suo interno si determina anche la configurazione del veicolo.

Il modello analitico e le procedure di esportazione della versione numerica in Matlab sono nello script *`model2W_2`*.

## Modello Matlab (work in progress) ##

Il modello Matlab presenta diversi file e funzioni generate tramite gli script Maple. Al suo interno i modelli presentati sono 2: il modello *dinamico* e il modello *quasi-steady-state*. Entrambi i modelli presentano 7 variabili di stato. Il modello quasi steady state Ã¨ utilizzato per l'estrazione del _GGplot_ e per le simulazioni di _minimum time manouver_.

 * Nella root del progetto ci sono alcuni script, tra cui `main.m`, che realizza tutta la simulazione fino alla produzione del gg-plot. In questa pagina sono presenti anche le funzioni 
 * `Include`: in questa directory sono salvate tutte le funzioni utilizzate nel progetto
 * `Scripts`: in questa directory sono presenti gli script accessori oltre a quelli principali, come alcuni script di plotting e di ricerca delle accelerazioni massime per il gg-plot.
 * `Simulink`: all'interno di questa directory ci sono i progetti simulink, tra i quali il modello a perfetta accelerazione longitudinale (`model3`),l'applicazione del controllo ottimo (`model4`) e il mesher automatico per il tracciato (`trackIntegrate`).
 * `Others`: vecchie implementazioni non sfruttate.

### Minimum time manouver ###

#### Only one forward ###

Tramite lo script `trackPlot` Ã¨ stato discretizzato il circuito in termini di lunghezza e curvatura. Sono stati creati i vettori `S_VECTOR` e `K_VECTOR`. `S_VECTOR` tiene conto della posizione dei vari punti in cui Ã¨ presente un nodo, `K_VECTOR` invece contiene la curvatura in corrispondenza di ogni nodo. Eâ€™ possibile decidere il nodo da cui far partire la simulazione, settando la variabile `n_start`, e il numero di nodi da prendere in considerazione, attraverso la variabile `n_nodes`. Il vettore `ds_VECTOR` contiene la differenza di posizione tra un nodo e quello successivo. A questo punto viene fatto partire un ciclo for che gira (`n_nodes-1`) volte. Al suo interno ha luogo un algoritmo di ottimizzazione: viene impostata una funzione che deve essere minimizzata, ottimizzando alcune variabili e imponendo determinati parametri. La funzione da massimizzare Ã¨ la velocitÃ  longitudinale della macchina (`VG`), mentre le variabili da ottimizzare sono `beta(t)`, `kappa_f(t)`, `kappa_r(t)`, `Nf(t)`, `Nr(t)`, `delta(t)` e `lc(t)`. Due condizioni sono stati impostati tramite le seguenti formule:

 * Accelerazione longitudinale: `Ax = du/ds * VG`
 * `Omega(t) = VG * curvatura`

La `du` Ã¨ stata espressa come differenza tra la velocitÃ  longitudinale da ottimizzare e quella ottimizzata del giro precedente. 
Un altro importante vincolo Ã¨ quello del _GGPLOT_: i risultati ottenuti in termini di velocitÃ  e accelerazione devono soddisfare i limiti imposti dal _GGPLOT_. Il modello utilizzato per la simulazione Ã¨ il Quasi-Steady-State Model.
Lâ€™algoritmo di ottimizzazione utilizzato `fmincon` ha bisogno di `boundaries` e di una `guess` iniziale. Come boundaries sono stati scelti dei valori fisicamente accettabili, mentre come guess sono state prese in considerazioni due diverse situazioni: nel caso in cui si stia considerando il primo nodo, la velocita iniziale e le altre guess sono impostate manualmente, nei nodi successivi le guess non sono altro che i valori ottimizzati del giro prima. In uscita viene creata una matrice `outTot` contenente i valori delle variabili ottimizzate ad ogni giro. 
Questo algoritmo Ã¨ sequenziale, nel senso che ottimizza le variabili nodo per nodo, non avendo una dimensione globale del problema: esso non sa cosa succede al nodo successivo, per cui puÃ² succedere che venga massimizzata la velocitÃ  poco prima di una curva e che non sia quindi in grado di affrontare la curva stessa. Questo algoritmo non fornisce quindi risultati veritieri, ma puÃ² fornire buoni risultati in rettilineo o come guess per altri algoritmi di ottimizzazione globali.

#### Backtracking Algorithms (aka Malato Edition) ####

Come nella prima soluzione proposta, anche questa sfrutta la stessa discretizzazione del tracciato.
L'algoritmo si basa su un ciclo al cui interno troviamo, il settaggio delle condizioni iniziali, i lowerbound (statici) e gli upperbound (dinamici). successivamente viene richiamato l'ottimizzatore, in base al risultato ottenuto dall'ottimizzatore possiamo avere tre possibili vie:

 * punto trovato: passo al successivo;
 * punto non trovato: diminuisco gli upperbound e riprovo a cercare una soluzione,
 * `upperbound < lowerbound`, non Ã¨ possibile trovare una soluzione in questo punto date le condizioni iniziali, torno al punto precedente, vario i suoi upperbound e cerco un'altra soluzione.

La funzione da minimizzare Ã¨ il tempo, mentre i parametri passati all'ottimizzatore sono: la velocitÃ  longitudinale della macchina (`VG`), `beta(t)`, `kappa_f(t)`, `kappa_r(t)`, `Nf(t)`, `Nr(t)`, `delta(t)` e `lc(t)`. 

Questo algoritmo Ã¨ sequenziale, nel senso che ottimizza le variabili nodo per nodo, non avendo una dimensione globale del problema: esso non sa cosa succede al nodo successivo, per cui puÃ² succedere che venga massimizzata la velocitÃ  poco prima di una curva e che non sia quindi in grado di affrontare la curva stessa. In questo caso interviene la parte di backtracking menzionata prima per fare altri tentativi sulle boundary conditions. 

#### Global problem (Full Time) ####

Il global time problem prevede l'utilizzo di funzioni obbiettivo e di vincolo uguali a quelle dell'algoritmo di backtrack, ma a differenza del precedente ha una visione globale del tracciato che deve percorrere. I singoli punti sono legati tra loro mediante il calcolo della velocitÃ  media e della curvatura media, che incide sulla accelerazione longitudinale e laterale del veicolo. La funzione obbiettivo da ottimizzare Ã¨ il tempo di percorso della parte di tracciato a cui sono aggiunti i residui della soluzione delle equazioni del modello.

Questa volta il numero di variabili da ottimizzare sono il prodotto tra il numero di variabili di ogni punto (la velocitÃ  longitudinale della macchina (`VG`), `beta(t)`, `kappa_f(t)`, `kappa_r(t)`, `Nf(t)`, `Nr(t)`, `delta(t)` e `lc(t)`) per il numero di punti di ottimizzazione. Questo rende l'algoritmo estremamente pesante, in modo esponenziale rispetto al numero di punti di ottimizzazione richiesti. Al limite, le equazioni di vincolo possono essere calcolate in forma parallela.
